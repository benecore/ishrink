#ifndef PLUGININTERFACE_H
#define PLUGININTERFACE_H

#include <QtPlugin>
#include <QObject>
#include <QString>

/**
 * @brief The PluginInterface class
 */
class PluginInterface : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief PluginInterface
     * @param parent
     */
    explicit PluginInterface(QObject *parent = 0) : QObject(parent) {}
    /**
     * @brief ~PluginInterface
     */
    virtual ~PluginInterface() {}

    /**
     * @brief Plugin name | example: MyGooglePlugin
     * @return plugin name
     */
    virtual QString name() const = 0;
    /**
     * @brief Plugin site | example: http://goo.gl
     * @return plugin site
     */
    virtual QString site() const = 0;
    /**
     * @brief Plugin version (major.minor.patch) | example: 0.0.1
     * @return plugin version
     */
    virtual QString version() const = 0;
    /**
     * @brief Author of the plugin (Pinocchio :D)
     * @return author
     */
    virtual QString author() const = 0;
    /**
     * @brief Short url
     * @param url
     */
    virtual void shortUrl(const QString &url) = 0;


signals:
    /**
     * @brief Emitted when request passed
     * @param response from server
     */
    void urlDone(const QByteArray response);
    /**
     * @brief Emitted when an error occurred
     * @param errorCode
     * @param errorString
     */
    void error(const int errorCode, const QString errorString);
};

QT_BEGIN_NAMESPACE

Q_DECLARE_INTERFACE(PluginInterface, "com.devpda.shortenerplugins/1.0")

QT_END_NAMESPACE


#endif // PLUGININTERFACE_H
