/*
 * pluginitem.cpp
 *
 *  Created on: 18.5.2014
 *      Author: benecore
 */

#include "pluginitem.h"

PluginItem::PluginItem(QObject *parent) :
QObject(parent),
_downloading(false),
_error(false),
_progress(0),
_speed("0/0 bytes/s"),
_installed(false),
_update(false)
{
}


PluginItem::PluginItem(const QVariantMap& _data, QObject* parent) :
        QObject(parent),
        data(_data),
        _downloading(false),
        _error(false),
        _progress(0),
        _speed("0/0 bytes/s"),
        _installed(false),
        _update(false)
{
}


PluginItem::~PluginItem()
{
}

