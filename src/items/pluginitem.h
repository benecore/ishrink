/*
 * pluginitem.h
 *
 *  Created on: 18.5.2014
 *      Author: benecore
 */

#ifndef PLUGINITEM_H_
#define PLUGINITEM_H_

#include <QObject>
#include <QVariantMap>
#include <QDebug>

class PluginItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant name READ name NOTIFY dataChanged)
    Q_PROPERTY(QVariant site READ site NOTIFY dataChanged)
    Q_PROPERTY(QVariant author READ author NOTIFY dataChanged)
    Q_PROPERTY(QVariant author_site READ author_site NOTIFY dataChanged)
    Q_PROPERTY(QVariant fileName READ fileName NOTIFY dataChanged)
    Q_PROPERTY(QVariant version READ version NOTIFY dataChanged)
    Q_PROPERTY(bool downloading READ downloading NOTIFY dataChanged)
    Q_PROPERTY(int progress READ progress WRITE setProgress NOTIFY dataChanged)
    Q_PROPERTY(QVariant speed READ speed WRITE setSpeed NOTIFY dataChanged)
    Q_PROPERTY(bool error READ error WRITE setError NOTIFY dataChanged)
    Q_PROPERTY(bool installed READ installed WRITE setInstalled NOTIFY dataChanged)
    Q_PROPERTY(bool update READ update WRITE setUpdate NOTIFY dataChanged)
public:
    PluginItem(QObject *parent = 0);
    PluginItem(const QVariantMap &_data, QObject *parent = 0);
    virtual ~PluginItem();


public slots:
	inline void resetDefault() {
		setProgress(0);
		setDownloading(false);
		setSpeed();
		setError(false);
		setInstalled(false);
	}
    inline QVariant name() const { return data.value("name"); }
    inline QVariant site() const { return data.value("site"); }
    inline QVariant author() const { return data.value("author"); }
    inline QVariant author_site() const { return data.value("author_site"); }
    inline QVariant fileName() const { return data.value("fileName"); }
    inline QVariant version() const { return data.value("version"); }
    inline bool downloading() const { return _downloading; }
    inline void setDownloading(const bool &value){
    	if (_downloading != value){
    		_downloading = value;
    		emit dataChanged();
    	}
    }
    inline int progress() const { return _progress; }
    inline void setProgress(const int &value){
        if (_progress != value){
            _progress = value;
            emit dataChanged();
        }
    }
    inline QVariant speed() const { return _speed; }
    inline void setSpeed(const QVariant &value = "0/0 bytes/s"){
        if (_speed != value){
            _speed = value;
            emit dataChanged();
        }
    }
    inline bool error() const { return _error; }
    inline void setError(const bool &value){
        if (_error != value){
            if (value){
                setProgress(0);
            }
            _error = value;
            emit dataChanged();
        }
    }
    inline bool installed() const { return _installed; }
    inline void setInstalled(const bool &value){
        if (_installed != value){
            _installed = value;
            emit dataChanged();
        }
    }
    inline bool update() const { return _update; }
    inline void setUpdate(const bool &value){
        if (_update != value){
            _update = value;
            emit dataChanged();
        }
    }

signals:
    void dataChanged();

private:
    QVariantMap data;
    bool _downloading;
    bool _error;
    int _progress;
    QVariant _speed;
    bool _installed;
    bool _update;
};

#endif /* PLUGINITEM_H_ */
