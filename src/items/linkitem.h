/*
 * linkitem.h
 *
 *  Created on: 20.5.2014
 *      Author: benecore
 */

#ifndef LINKITEM_H_
#define LINKITEM_H_

#include <QObject>

class LinkItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY dataChanged)
    Q_PROPERTY(QString shortUrl READ shortUrl WRITE setShortUrl NOTIFY dataChanged)
    Q_PROPERTY(QString longUrl READ longUrl WRITE setLongUrl NOTIFY dataChanged)
    Q_PROPERTY(QString tags READ tags WRITE setTags NOTIFY dataChanged)
public:
    LinkItem(QObject *parent = 0);
    virtual ~LinkItem();



public slots:
    inline void setDefaults(){
        _title = "";
        _shortUrl = "";
        _longUrl = "";
        _tags = "";
    }
    inline QString title() const { return _title; }
    inline void setTitle(const QString &value){
        if (_title != value){
            _title = value;
            emit dataChanged();
        }
    }
    inline QString shortUrl() const { return _shortUrl; }
    inline void setShortUrl(const QString &value){
        if (_shortUrl != value){
            _shortUrl = value;
            emit dataChanged();
        }
    }
    inline QString longUrl() const { return _longUrl; }
    inline void setLongUrl(const QString &value){
        if (_longUrl != value){
            _longUrl = value;
            emit dataChanged();
        }
    }
    inline QString tags() const { return _tags; }
    inline void setTags(const QString &value){
        if (_tags != value){
            _tags = value;
            emit dataChanged();
        }
    }


signals:
    void dataChanged();


private:
    QString _title;
    QString _shortUrl;
    QString _longUrl;
    QString _tags;

};

#endif /* LINKITEM_H_ */
