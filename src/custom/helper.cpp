/*
 * helper.cpp
 *
 *  Created on: 21.5.2014
 *      Author: benecore
 */

#include "helper.h"

Helper *Helper::_instance = 0;

Helper::Helper(QObject *parent) :
        QObject(parent),
        invoker(0),
        toast(0)
{
}

Helper::~Helper()
{
}


Helper *Helper::instance()
{
    if (!_instance)
        _instance = new Helper;
    return _instance;
}


void Helper::destroy()
{
    if (_instance){
        delete _instance;
        _instance = 0;
    }
}

void Helper::copyText(const QByteArray& text)
{
    Clipboard clipboard;
    clipboard.clear();
    bool check = clipboard.insert("text/plain", text);
    if (check){
        showToast(tr("Copied"));
    }
}

void Helper::showToast(const QString& message)
{
    toast = new SystemToast(this);
    toast->setPosition(SystemUiPosition::TopCenter);
    connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)), toast, SLOT(deleteLater()));
    toast->button()->setLabel("");
    toast->setBody(message);
    toast->show();
}

void Helper::openBrowser(const QString& url)
{
    qDebug() << Q_FUNC_INFO;
    invoker = new Invoker(this);
    connect(invoker, SIGNAL(invocationFinished()), invoker, SLOT(deleteLater()));
    connect(invoker, SIGNAL(invocationFailed()), invoker, SLOT(deleteLater()));
    invoker->setTarget("sys.browser");
    invoker->setAction("bb.action.OPEN");
    invoker->setUri(url);
    invoker->invoke();
}

void Helper::share(const QString& data)
{
    _invocation = Invocation::create(
            InvokeQuery::create()
    .parent(this)
    .mimeType("text/plain")
    .data(data.toUtf8()));

    connect(_invocation, SIGNAL(armed()),
            this, SLOT(armed()));
    connect(_invocation, SIGNAL(finished()),
            _invocation, SLOT(deleteLater()));
}

void Helper::armed()
{
    _invocation->trigger("bb.action.SHARE");
}
