/*
 * Invoker.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Martin R. Green
 *     Licence: CC By 3.0 (http://creativecommons.org/licenses/by/3.0)
 *
 */

#include "invoker.h"

Invoker::Invoker( QObject* parent ) :
        BaseObject( parent ) {
    this->_lastError = InvokeReplyError::None;

    try {
        this->_request = new InvokeRequest();

    } catch ( ... ) {
        this->_request = NULL;
    }
}

Invoker::~Invoker() {
    if( this->_request )
        delete this->_request;
}

void Invoker::invoke() {
    if ( this->_request ) {
        InvokeManager* manager = new InvokeManager();
        InvokeTargetReply* reply = manager->invoke( *this->_request );
        if ( reply ) {
            bool success;
            Q_UNUSED( success )

            // ---Make sure the manager gets deleted when the invocation reply is finished processing
            success = QObject::connect( reply, SIGNAL(finished()), manager, SLOT(deleteLater()) );
            Q_ASSERT( success );

            // ---Allow error checking and cleanup when the invocation reply is finished processing
            success = QObject::connect( reply, SIGNAL(finished()), this, SLOT(invokeReplyFinishedHandler()) );
            Q_ASSERT( success );
        }
    }
}

void Invoker::invokeSlot() {
    this->invoke();
}

void Invoker::invokeReplyFinishedHandler() {
    InvokeTargetReply* reply = qobject_cast<InvokeTargetReply*>( sender() );
    if ( ( this->_lastError = reply->error() ) != InvokeReplyError::None ) {
        emit invocationFailed();
    } else {
        emit invocationFinished();
    }
    reply->deleteLater();
}

// ---Accessors
bool Invoker::valid() {
    return this->_request;
}

InvokeReplyError::Type Invoker::lastError() {
    return this->_lastError;
}

QString Invoker::lastErrorText() {
    switch ( this->_lastError ) {
        case InvokeReplyError::BadRequest:
            return "Bad Request";
        case InvokeReplyError::InsufficientPrivileges:
            return "Insufficient Privileges";
        case InvokeReplyError::Internal:
            return "Internal Error";
        case InvokeReplyError::NoTarget:
            return "No Target Found";
        case InvokeReplyError::Target:
            return "Target Failed";
        case InvokeReplyError::TargetNotOwned:
            return "Target Not Owned";
        case InvokeReplyError::None:
            break;
    }
    return "";
}

QString Invoker::action() {
    return ( this->_request ) ? this->_request->action() : QString::null;
}

QString Invoker::setAction( const QString& action ) {
    if ( this->_request ) {
        QString oldVal = this->_request->action();
        this->_request->setAction( action );
        return oldVal;
    } else
        return QString::null;
}

QUrl Invoker::uri() const {
    return ( this->_request ) ? this->_request->uri() : QUrl();
}

QUrl Invoker::setUri( const QUrl& uri ) const {
    if ( this->_request ) {
        QUrl oldUri = this->_request->uri();
        this->_request->setUri( uri );
        return oldUri;
    } else
        return QUrl();
}

QString Invoker::setUri( const QString& uri ) const {
    if ( this->_request ) {
        QUrl oldUri = this->_request->uri();
        this->_request->setUri( uri );
        return oldUri.toString();
    } else
        return QString::null;
}

char* Invoker::setUri( const char* uri ) const {
    if ( this->_request ) {
        QUrl oldUri = this->_request->uri();
        this->_request->setUri( uri );
        return oldUri.toString().toLatin1().data();
    } else
        return NULL;
}

QString Invoker::target() const {
    return ( this->_request ) ? this->_request->target() : QString::null;
}

QString Invoker::setTarget( const QString& target ) const {
    if ( this->_request ) {
        QString oldTarget = this->_request->target();
        this->_request->setTarget( target );
        return oldTarget;
    } else
        return QString::null;
}

QByteArray Invoker::data() {
    if ( this->_request ) {
        return this->_request->data();
    } else
        return QByteArray();
}

QByteArray Invoker::setData( const QByteArray& data ) {
    if ( this->_request ) {
        QByteArray oldData = this->_request->data();
        this->_request->setData( data );
        return oldData;
    } else
        return QByteArray();
}

FileTransferMode::Type Invoker::fileTransferMode() {
    return ( this->_request ) ? this->_request->fileTransferMode() : FileTransferMode::Unspecified;
}

FileTransferMode::Type Invoker::setFileTransferMode( FileTransferMode::Type fileTransferMode ) {
    if ( this->_request ) {
        FileTransferMode::Type oldMode = this->_request->fileTransferMode();
        this->_request->setFileTransferMode( fileTransferMode );
        return oldMode;
    } else
        return FileTransferMode::Unspecified;
}

int Invoker::listId() {
    return ( this->_request ) ? this->_request->listId() : 0;
}

int Invoker::setListId( int listId ) {
    if ( this->_request ) {
        int oldId = this->_request->listId();
        this->_request->setListId( listId );
        return oldId;
    } else
        return 0;
}

QVariantMap Invoker::metaData() const {
    return ( this->_request ) ? this->_request->metadata() : QVariantMap();
}

QVariantMap Invoker::setMetaData( const QVariantMap& metaData ) const {
    if ( this->_request ) {
        QVariantMap oldMetaData = this->_request->metadata();
        this->_request->setMetadata( metaData );
        return oldMetaData;
    } else
        return QVariantMap();
}

QString Invoker::mimeType() const {
    return ( this->_request ) ? this->_request->mimeType() : QString::null;
}

QString Invoker::setMimeType( const QString& mimeType ) const {
    if ( this->_request ) {
        QString oldMimeType = this->_request->mimeType();
        this->_request->setMimeType( mimeType );
        return oldMimeType;
    } else
        return QString::null;
}

SecurityPerimeter::Type Invoker::perimeter() {
    return ( this->_request ) ? this->_request->perimeter() : SecurityPerimeter::Default;
}

SecurityPerimeter::Type Invoker::setPerimeter( SecurityPerimeter::Type perimeter ) {
    if ( this->_request ) {
        SecurityPerimeter::Type oldPerimeter = this->_request->perimeter();
        this->_request->setPerimeter( perimeter );
        return oldPerimeter;
    } else
        return SecurityPerimeter::Default;
}

InvokeTarget::Types Invoker::targetTypes() {
    return ( this->_request ) ? this->_request->targetTypes() : InvokeTarget::Unspecified;
}

InvokeTarget::Types Invoker::setTargetTypes( InvokeTarget::Types targetTypes ) {
    if ( this->_request ) {
        InvokeTarget::Types oldTypes = this->_request->targetTypes();
        this->_request->setTargetTypes( targetTypes );
        return oldTypes;
    } else
        return InvokeTarget::Unspecified;
}
