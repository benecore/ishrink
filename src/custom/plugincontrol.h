/*
 * plugin.h
 *
 *  Created on: 20.5.2014
 *      Author: benecore
 */

#ifndef PLUGINCONTROL_H_
#define PLUGINCONTROL_H_

#include <QObject>
#include <QPluginLoader>
#include <QtNetwork>
#include <bb/cascades/Label>
#include <bb/data/JsonDataAccess>
#include <bb/system/SystemProgressToast>
#include <bb/system/SystemUiResult>
#include <bb/system/SystemDialog>


#include "../downloader.h"
#include "../items/pluginitem.h"
#include "../interface/plugininterface.h"
#include "helper.h"
#include "../storage/database.h"

using namespace bb::data;
using namespace bb::system;
using namespace bb::cascades;

class PluginControl : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool loading READ loading WRITE setLoading NOTIFY loadingChanged)
    Q_PROPERTY(QVariantList installed READ installed CONSTANT)
public:
    PluginControl(QObject *parent = 0);
    virtual ~PluginControl();

    static PluginControl *instance();
    static void destroy();


    Q_INVOKABLE
    void getPlugins();
    Q_INVOKABLE
    void loadPlugins();
    Q_INVOKABLE
    QVariantList installed();
    Q_INVOKABLE
    void installPlugin(QObject *object);
    Q_INVOKABLE
    void updatePlugin(QObject *object);
    Q_INVOKABLE
    void uninstallPlugin(QObject *object);
    Q_INVOKABLE
    void shortUrl(const QString &interfaceName, const QString &url);

    Q_INVOKABLE
    void setLabel(bb::cascades::Label *label);


signals:
    void urlDone(const QByteArray response);
    void installedChanged();
    void pluginDone(PluginItem *plugin);
    void loadingChanged(bool loading);
    void serverError(const int errorCode, const QString errorString);


private slots:
    void urlFinished(const QByteArray response);
    void error(int errorCode, QString errorString);
    inline bool loading() const { return _loading; }
    inline void setLoading(const bool &value){
        if (_loading != value){
            _loading = value;
            emit loadingChanged(_loading);
        }
    }
    void finished(QNetworkReply *reply);
    void activatePlugin(const QString& fileName, PluginItem *plugin);


    private:
    Q_DISABLE_COPY(PluginControl)
    static PluginControl *_instance;
    QNetworkAccessManager *_manager;
    QHash<QString, PluginInterface*> loadedPlugins;
    bool _loading;
    Label *_shortUrlLabel;
};

#endif /* PLUGINCONTROL_H_ */
