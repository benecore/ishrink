/*
 * database.cpp
 *
 *  Created on: 7.3.2014
 *      Author: benecore
 */

#include "database.h"

Database *Database::_instance = 0;

Database::Database()
{
    db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
    db->setDatabaseName("data/database.db");
    bool ok = db->open();
    if(ok){
        qDebug() << "DatabaseManager: db opened.";
    }else{
        qDebug() << "DatabaseManager: db open error.";
    }
    createTables();
}

Database::~Database()
{
    delete db;
}


Database *Database::instance()
{
    if (!_instance)
        _instance = new Database;
    return _instance;
}


void Database::destroy()
{
    if (_instance){
        delete _instance;
        _instance = 0;
    }
}


void Database::createTables()
{
    db->exec("CREATE TABLE IF NOT EXISTS links(title TEXT, shortUrl TEXT UNIQUE, longUrl TEXT, tags TEXT, timestamp TEXT)");
    db->exec("CREATE TABLE IF NOT EXISTS defaultPlugin(plugin TEXT)");
    db->exec("CREATE TABLE IF NOT EXISTS language(language TEXT)");
    db->exec("CREATE TABLE IF NOT EXISTS shareText(text TEXT)");
    db->exec("CREATE TABLE IF NOT EXISTS pluginCount(count NUMBER)");
}

bool Database::addLink(const QString& title,
        const QString& shortUrl,
        const QString& longUrl,
        const QString& tags)
{
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.prepare("INSERT INTO links VALUES(?, ?, ?, ?, ?)");
    qry.addBindValue(title);
    qry.addBindValue(shortUrl);
    qry.addBindValue(longUrl);
    qry.addBindValue(tags);
    qry.addBindValue(QDateTime::currentDateTime().toTime_t());

    bool check = qry.exec();

    if (check)
        emit dataChanged();
    return check;
}

bool Database::removeLink(const QString& shortUrl)
{
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.prepare("REMOVE FROM links WHERE shortUrl = ?");
    qry.addBindValue(shortUrl);

    bool check = qry.exec();
    if (check)
        emit dataChanged();
    return check;

}

QVariantList Database::getLinks()
{
    QVariantList list;
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.prepare("SELECT * FROM links");

    qry.exec();
    while(qry.next()){
        QVariantMap map;
        map.insert("title", qry.value(0));
        map.insert("shortUrl", qry.value(1));
        map.insert("longUrl", qry.value(2));
        map.insert("tags", qry.value(3));
        map.insert("timestamp", qry.value(4));
        list.append(map);
    }
    return list;
}

bool Database::exists(const QString& longUrl)
{
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.prepare("SELECT longUrl FROM links WHERE longUrl = ?");
    qry.addBindValue(longUrl);

    qry.exec();

    return (qry.record().count() != 0);
}

QString Database::defaultPlugin() const
{
    QString plugin;
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;

    qry.exec("SELECT * FROM defaultPlugin");

    while(qry.next()){
        plugin = qry.value(0).toString();
    }
    return plugin;
}

void Database::setDefaultPlugin(const QString& plugin)
{
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.exec("REMOVE * FROM defaultPlugin");
    qry.prepare("INSERT INTO defaultPlugin VALUES(?)");
    qry.addBindValue(plugin);

    bool check = qry.exec();
    if (!check)
        qWarning() << "Unable set default plugin" << qry.lastError().text();
    else
        emit dataChanged();
}

QString Database::language() const
{
    QString language;
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.exec("SELECT * FROM language");

    while(qry.next()){
        language = qry.value(0).toString();
    }
    if (language.isEmpty())
        return QString("en");
    return language;
}

void Database::setLanguage(const QString& language)
{
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.exec("REMOVE * FROM language");
    qry.prepare("INSERT INTO language VALUES(?)");
    qry.addBindValue(language);

    bool check = qry.exec();
    if (!check)
        qWarning() << "Unable set language" << qry.lastError().text();
    else
        emit dataChanged();
}

QString Database::shareText() const
{
    QString text;
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.exec("SELECT * FROM shareText");

    while(qry.next()){
        text = qry.value(0).toString();
    }
    return text;
}

void Database::setShareText(const QString& shareText)
{
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.exec("REMOVE * FROM shareText");
    qry.prepare("INSERT INTO shareText VALUES(?)");
    qry.addBindValue(shareText);

    bool check = qry.exec();
    if (!check)
        qWarning() << "Unable set shareText" << qry.lastError().text();
    else
        emit dataChanged();
}

int Database::pluginCount() const
{
    int count;
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.exec("SELECT * FROM pluginCount");

    while(qry.next()){
        count = qry.value(0).toInt();
    }
    return count;
}

void Database::setPluginCount(const int& pluginCount)
{
    if (!db->isOpen())
        db->open();
    QSqlQuery qry;
    qry.exec("REMOVE * FROM pluginCount");
    qry.prepare("INSERT INTO pluginCount VALUES(?)");
    qry.addBindValue(pluginCount);

    bool check = qry.exec();
    if (!check)
        qWarning() << "Unable set pluginCount" << qry.lastError().text();
    else
        emit dataChanged();
}
