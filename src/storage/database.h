/*
 * database.h
 *
 *  Created on: 7.3.2014
 *      Author: benecore
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include <QObject>
#include <QtSql>
#include <QList>
#include <QDebug>
#include <bb/cascades/CustomControl>
#include <QDateTime>
using namespace bb::cascades;

class Database : public bb::cascades::CustomControl
{
	Q_OBJECT
	Q_PROPERTY(QString language READ language WRITE setLanguage NOTIFY dataChanged)
	Q_PROPERTY(QVariantList links READ getLinks NOTIFY dataChanged)
	Q_PROPERTY(QString defaultPlugin READ defaultPlugin WRITE setDefaultPlugin NOTIFY dataChanged)
	Q_PROPERTY(QString shareText READ shareText WRITE setShareText NOTIFY dataChanged)
	Q_PROPERTY(int pluginCount READ pluginCount WRITE setPluginCount NOTIFY dataChanged)
public:
	Database();
	virtual ~Database();

	static Database *instance();
	static void destroy();


	Q_INVOKABLE
	bool addLink(const QString &title,
	        const QString &shortUrl,
	        const QString &longUrl,
	        const QString &tags);
	Q_INVOKABLE
	bool removeLink(const QString &shortUrl);
	Q_INVOKABLE
	QVariantList getLinks();
	Q_INVOKABLE
	bool exists(const QString &longUrl);

	// Plugin default
	QString defaultPlugin() const;
	void setDefaultPlugin(const QString &plugin);

	QString language() const;
	void setLanguage(const QString &language);
	/**
	 * Predefined share text
	 */
	QString shareText() const;
	void setShareText(const QString &shareText);


	int pluginCount() const;
	void setPluginCount(const int &pluginCount);

protected:
	void createTables();


signals:
    void dataChanged();

private:
	Q_DISABLE_COPY(Database)
	static Database *_instance;
	QSqlDatabase *db;
};

#endif /* DATABASE_H_ */
