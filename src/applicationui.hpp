/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>
#include <QtNetwork>
#include <bb/device/HardwareInfo>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeRequest>
#include <bb/cascades/TextField>

#include "livecoding/qmlbeam.h"
#include "items/pluginitem.h"
#include "items/linkitem.h"
#include "storage/database.h"


#include "custom/plugincontrol.h"
#include "custom/helper.h"
#include "custom/frame.h"

/** Platform features **/
#include "platform/InviteToDownload.hpp"
#include "platform/RegistrationHandler.hpp"
#include "platform/StatusEventHandler.h"

using namespace bb::device;
using namespace bb::platform;
using namespace bb::system;
using namespace bb::cascades;

namespace bb
{
    namespace cascades
    {
        class Application;
        class LocaleHandler;
        class Label;
    }
}

class QTranslator;

/*!
 * @brief Application object
 *
 *
 */

class ApplicationUI : public QObject
{
    Q_OBJECT
public:
    ApplicationUI(bb::cascades::Application *app);
    virtual ~ApplicationUI();

    Q_INVOKABLE
    void changeLanguage(const QString& language);

private slots:
    void onSystemLanguageChanged();
    void onInvoke(const bb::system::InvokeRequest& invoke);

private:
    QTranslator* m_pTranslator;
    bb::cascades::LocaleHandler* m_pLocaleHandler;
    bb::cascades::Label *shortUrlLabel;
    /* Platform */
    RegistrationHandler *_registrationHandler;
    InviteToDownload *_inviteDownload;
    InvokeManager *_invokeManager;
    TextField *urlTextField;

};

#endif /* ApplicationUI_HPP_ */
