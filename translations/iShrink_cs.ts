<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../assets/720x720/AboutPage.qml" line="36"/>
        <location filename="../assets/AboutPage.qml" line="36"/>
        <source>Thanks</source>
        <translation>Děkuji</translation>
    </message>
    <message>
        <location filename="../assets/720x720/AboutPage.qml" line="84"/>
        <location filename="../assets/AboutPage.qml" line="84"/>
        <source>Version: %1</source>
        <translation>Verze: %1</translation>
    </message>
    <message>
        <location filename="../assets/720x720/AboutPage.qml" line="97"/>
        <location filename="../assets/AboutPage.qml" line="97"/>
        <source>Keep iShrink free</source>
        <translation>Udržujte iShrink zdarma</translation>
    </message>
    <message>
        <location filename="../assets/720x720/AboutPage.qml" line="125"/>
        <location filename="../assets/AboutPage.qml" line="125"/>
        <source>Developer</source>
        <translation>Vývojář</translation>
    </message>
    <message>
        <location filename="../assets/720x720/AboutPage.qml" line="159"/>
        <location filename="../assets/AboutPage.qml" line="157"/>
        <source>In case of any problems, please contact me</source>
        <translation>V případě jakýchkoliv problémů, prosím, kontaktujte mě</translation>
    </message>
    <message>
        <location filename="../assets/720x720/AboutPage.qml" line="177"/>
        <location filename="../assets/AboutPage.qml" line="176"/>
        <source>Translators</source>
        <translation>Překladatelé</translation>
    </message>
    <message>
        <location filename="../assets/720x720/AboutPage.qml" line="178"/>
        <location filename="../assets/AboutPage.qml" line="177"/>
        <source>Thanks to</source>
        <translation>Poděkování</translation>
    </message>
    <message>
        <location filename="../assets/720x720/AboutPage.qml" line="183"/>
        <location filename="../assets/AboutPage.qml" line="182"/>
        <source>Chinese</source>
        <translation>Čínština</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="obsolete">Španeilčina</translation>
    </message>
</context>
<context>
    <name>ApplicationUI</name>
    <message>
        <source>shared with #iShrink</source>
        <translation type="obsolete">sdílené s #iShrink</translation>
    </message>
    <message>
        <source>shared using #iShrink app</source>
        <translation type="obsolete">sdílené pomocí #iShrink aplikace</translation>
    </message>
</context>
<context>
    <name>Helper</name>
    <message>
        <location filename="../src/custom/helper.cpp" line="46"/>
        <source>Copied</source>
        <translation>Zkopírovano</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../assets/720x720/HomePage.qml" line="38"/>
        <location filename="../assets/HomePage.qml" line="38"/>
        <source>The url is blocked by URL.IE</source>
        <translation>Adresa je blokována serverem</translation>
    </message>
    <message>
        <location filename="../assets/720x720/HomePage.qml" line="54"/>
        <location filename="../assets/HomePage.qml" line="54"/>
        <source>Select server first</source>
        <translation>Nejprve vyberte server</translation>
    </message>
    <message>
        <location filename="../assets/720x720/HomePage.qml" line="63"/>
        <location filename="../assets/HomePage.qml" line="63"/>
        <source>Create link</source>
        <translation>Zkrátit adresu</translation>
    </message>
    <message>
        <location filename="../assets/720x720/HomePage.qml" line="76"/>
        <location filename="../assets/HomePage.qml" line="76"/>
        <source>Servers</source>
        <translation>Servery</translation>
    </message>
    <message>
        <location filename="../assets/720x720/HomePage.qml" line="127"/>
        <location filename="../assets/HomePage.qml" line="126"/>
        <source>Active server</source>
        <translation>Aktivní server</translation>
    </message>
    <message>
        <location filename="../assets/720x720/HomePage.qml" line="148"/>
        <location filename="../assets/HomePage.qml" line="147"/>
        <source>Invalid url</source>
        <translation>Neplatná adresa</translation>
    </message>
</context>
<context>
    <name>ListItemPlugin</name>
    <message>
        <location filename="../assets/720x720/items/ListItemPlugin.qml" line="120"/>
        <location filename="../assets/items/ListItemPlugin.qml" line="121"/>
        <source>Download failed. Try again later. If the problem persists, contact the developer</source>
        <translation>Stažení se nezdařilo. Zkuste to znovu později. Pokud problém přetrvává, obraťte se na vývojáře</translation>
    </message>
</context>
<context>
    <name>PluginControl</name>
    <message>
        <location filename="../src/custom/plugincontrol.cpp" line="61"/>
        <source>Installing %1</source>
        <translation>Instalace %1</translation>
    </message>
    <message>
        <location filename="../src/custom/plugincontrol.cpp" line="94"/>
        <source>Updating %1</source>
        <translation>Aktualizace %1</translation>
    </message>
    <message>
        <location filename="../src/custom/plugincontrol.cpp" line="125"/>
        <source>Plugin uninstalled</source>
        <translation>Server odinstalován</translation>
    </message>
    <message>
        <location filename="../src/custom/plugincontrol.cpp" line="152"/>
        <source>Server error: %1
Try again later</source>
        <translation>Chyba serveru: %1
Zkuste to znovu později</translation>
    </message>
    <message>
        <location filename="../src/custom/plugincontrol.cpp" line="184"/>
        <source>New servers available</source>
        <translation>Nové servery k dispozici</translation>
    </message>
</context>
<context>
    <name>PluginsAction</name>
    <message>
        <location filename="../assets/actions/PluginsAction.qml" line="11"/>
        <source>Servers</source>
        <translation>Servery</translation>
    </message>
</context>
<context>
    <name>PluginsPage</name>
    <message>
        <location filename="../assets/720x720/PluginsPage.qml" line="17"/>
        <location filename="../assets/PluginsPage.qml" line="17"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../assets/720x720/PluginsPage.qml" line="24"/>
        <location filename="../assets/PluginsPage.qml" line="24"/>
        <source>Updates available</source>
        <translation>Dostupná aktualizace</translation>
    </message>
    <message>
        <location filename="../assets/720x720/PluginsPage.qml" line="30"/>
        <location filename="../assets/PluginsPage.qml" line="30"/>
        <source>Servers</source>
        <translation>Servery</translation>
    </message>
    <message>
        <location filename="../assets/720x720/PluginsPage.qml" line="75"/>
        <location filename="../assets/PluginsPage.qml" line="75"/>
        <source>Uninstall</source>
        <translation>Odinstalovat</translation>
    </message>
    <message>
        <location filename="../assets/720x720/PluginsPage.qml" line="76"/>
        <location filename="../assets/PluginsPage.qml" line="76"/>
        <source>Update</source>
        <translation>Aktualizovat</translation>
    </message>
    <message>
        <location filename="../assets/720x720/PluginsPage.qml" line="76"/>
        <location filename="../assets/PluginsPage.qml" line="76"/>
        <source>Install</source>
        <translation>Instalovat</translation>
    </message>
    <message>
        <location filename="../assets/720x720/PluginsPage.qml" line="77"/>
        <location filename="../assets/PluginsPage.qml" line="77"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>RefreshAction</name>
    <message>
        <location filename="../assets/actions/RefreshAction.qml" line="11"/>
        <source>Refresh</source>
        <translation>Obnovit</translation>
    </message>
</context>
<context>
    <name>RegistrationHandler</name>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="37"/>
        <source>Please wait while the application connects to BBM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="116"/>
        <source>Application connected to BBM.  Press Continue.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="128"/>
        <source>Disconnected by RIM. RIM is preventing this application from connecting to BBM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="134"/>
        <source>Disconnected. Go to Settings -&gt; Security and Privacy -&gt; Application Permissions and connect this application to BBM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="142"/>
        <source>Invalid UUID. Report this error to the vendor.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="148"/>
        <source>Too many applications are connected to BBM. Uninstall one or more applications and try again.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="156"/>
        <source>Cannot connect to BBM. Download this application from AppWorld to keep using it.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="162"/>
        <source>Check your Internet connection and try again.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="169"/>
        <source>Connecting to BBM. Please wait.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="174"/>
        <source>Determining the status. Please wait.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="184"/>
        <source>Would you like to connect the application to BBM?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../assets/SettingsPage.qml" line="8"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="37"/>
        <source>Current language</source>
        <translation>Jazyk aplikace</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="39"/>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="45"/>
        <source>Chinese</source>
        <translation>Čínština</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="51"/>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="57"/>
        <source>Slovak</source>
        <translation>Slovenčina</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="72"/>
        <source>Saved</source>
        <translation>Uloženo</translation>
    </message>
    <message>
        <location filename="../assets/SettingsPage.qml" line="76"/>
        <source>enter custom prefix text to share</source>
        <translation>zadejte vlastní prefix text pro sdílení</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>shared using #iShrink app</source>
        <translation type="obsolete">sdílené pomocí #iShrink aplikace</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="31"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="39"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="46"/>
        <source>Rate</source>
        <translation>Ohodnotit</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="56"/>
        <source>Tell a Friend</source>
        <translation>Odporučit</translation>
    </message>
</context>
</TS>
