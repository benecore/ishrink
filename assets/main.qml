/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.3
import com.devpda.tools 1.3

import "actions"

NavigationPane {
    id: pane
    
    //property string sharedText: qsTr("shared using #iShrink app") + Retranslate.onLocaleOrLanguageChanged
    property string sharedText: "#iShrink"
    
    Menu.definition: MenuDefinition {
        helpAction: HelpActionItem {
            id: aboutAction
            title: qsTr("About") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/info.png"
            onTriggered: {
                push(aboutPage.createObject())
            }
        }
        settingsAction: SettingsActionItem {
            id: settingsAction
            title: qsTr("Settings") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                push(settingsPage.createObject())
            }
        }
        actions: [
            ActionItem {
                title: qsTr("Rate") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///images/rate.png"
                onTriggered: {
                    invoker.action = "bb.action.OPEN"
                    invoker.target = "sys.appworld"
                    invoker.uri = "appworld://content/56167890"
                    invoker.invoke()
                }
            },
            ActionItem {
                title: qsTr("Tell a Friend") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///images/invite-download.png"
                onTriggered: {
                    if (BBM.allowed)
                        Invite.sendInvite();
                    else
                        BBM.registerApplication()
                }
            }
        ]
    }
    
    attachedObjects: [
        ComponentDefinition {
            id: dynamicPlugins
            PluginsPage {}
        },
        ComponentDefinition {
            id: settingsPage
            SettingsPage{}
        },
        ComponentDefinition {
            id: aboutPage
            AboutPage {}
        },
        Invoker {
            id: invoker
            onInvocationFailed: {
                console.log("Invokation failed")
            }
        }
    ]
    
    onPopTransitionEnded: {
        page.active = false 
        page.destroy()
    }
    
    onPushTransitionEnded: {
        page.active = true
    }
    
    
    HomePage {
    }
    
    
    onCreationCompleted: {
        if (!BBM.allowed)
            BBM.registerApplication()
        console.log("Links: "+Links.jsonFile)
        PluginControl.loadPlugins()
        //App.loadPlugins()
    }
}
