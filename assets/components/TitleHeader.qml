import bb.cascades 1.3

TitleBar {
    id: root
    
    kind: TitleBarKind.FreeForm
    kindProperties: FreeFormTitleBarKindProperties {
        Container {
            id: rootContainer
            layout: DockLayout{}
            background: Color.create("#f5f5f5")
            leftPadding: 15
            rightPadding: 15
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                verticalAlignment: VerticalAlignment.Center
                
                ImageView {
                    verticalAlignment: VerticalAlignment.Center
                    imageSource: "asset:///images/header-image.amd"
                }
                Label {
                    verticalAlignment: VerticalAlignment.Center
                    textStyle{
                        fontSize: FontSize.PointValue
                        fontSizeValue: 10
                        fontWeight: FontWeight.W500
                    }
                    text: root.title
                }
            } // end of Icon/Label container
            
        } // end of root container
    } // end of Free Form
} // end of Title Bar