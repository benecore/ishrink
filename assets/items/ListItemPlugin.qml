import bb.cascades 1.2

Container {
    id: root
    minHeight: 150
    topMargin: 20
    leftPadding: 30
    rightPadding: 30
    topPadding: 30
    bottomPadding: 30
    //background: root.ListItem.selected ? Color.create("#d0d0d0") : Color.create("#f0f0f0")
    background: paint.imagePaint
    scaleX: root.ListItem.active ? 0.97 : 1
    scaleY: scaleX
    layout: DockLayout{}
    
    attachedObjects: [
        ImagePaintDefinition {
            id: paint
            imageSource: "asset:///images/image_border.png"
        }
    ]
    
    Container {
        horizontalAlignment: HorizontalAlignment.Right
        verticalAlignment: VerticalAlignment.Center
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        ImageView {
            opacity: 0.5
            leftMargin: 0
            rightMargin: 0
            topMargin: 0
            bottomMargin: 0
            visible: ListItemData.installed
            imageSource: "asset:///images/installed.png"
            preferredHeight: 70
            scalingMethod: ScalingMethod.AspectFit
        }
        
        ImageView {
            opacity: 0.5
            leftMargin: 0
            rightMargin: 0
            topMargin: 0
            bottomMargin: 0
            visible: ListItemData.update
            imageSource: "asset:///images/plugin-update.png"
            preferredHeight: 70
            scalingMethod: ScalingMethod.AspectFit
        }
    }
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Fill
            
            Container {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                horizontalAlignment: HorizontalAlignment.Fill
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        textStyle{
                            fontSize: FontSize.PointValue
                            fontSizeValue: 10
                        }
                        verticalAlignment: VerticalAlignment.Center
                        text: ListItemData.name 
                        bottomMargin: 0
                    }
                    Label {
                        verticalAlignment: VerticalAlignment.Center
                        text: "(v"+ListItemData.version+")"
                        textStyle{
                            fontSize: FontSize.PointValue
                            fontSizeValue: 6
                        }
                    }
                }
                
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Fill
                    Label {
                        text: ListItemData.site
                        textStyle{
                            fontSize: FontSize.PointValue
                            fontSizeValue: 6
                        }
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                    }
                } // end of Site
            }
        }
        Label {
            visible: ListItemData.error
            topMargin: 0
            multiline: true
            verticalAlignment: VerticalAlignment.Center
            textStyle{
                fontSize: FontSize.PointValue
                fontSizeValue: 6
                color: Color.Red
            }
            text: qsTr("Download failed. Try again later. If the problem persists, contact the developer") + Retranslate.onLocaleOrLanguageChanged
        }
    } // end of StackLayout container
} // end of root