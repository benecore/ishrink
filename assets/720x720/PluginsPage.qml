import bb.cascades 1.3
import bb.system 1.2

import "items"
import "components"
import "actions"

BasePage {
    id: root
    
    function response(plugin){
        model.append(plugin)
    }
    
    
    function serverError(code, error){
        errorLabel.text = qsTr("Error") + Retranslate.onLocaleOrLanguageChanged + ": "+code +"\nTry again later";
        errorLabel.visible = true 
    }
    
    function updateAvailable(value){
        if (value === true){
            App.pluginUpdate = false
            toast.body = qsTr("Updates available") + Retranslate.onLocaleOrLanguageChanged
            toast.show()
        }
    }
    
    property variant indexPath: 0
    titleText: qsTr("Servers") + Retranslate.onLocaleOrLanguageChanged
    
    
    onCreationCompleted: {
        PluginControl.pluginDone.connect(response)
        PluginControl.serverError.connect(serverError)
    }


    onActiveChanged: {
        if (active){
            PluginControl.getPlugins()
        }
    }
    
    
    actions: [
        RefreshAction {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                errorLabel.visible = false
                PluginControl.getPlugins()
                model.clear()
            }
        }
    ]
    
    
    attachedObjects: [
        SystemDialog {
            id: dialog
            property variant plugin: null
            title: plugin ? plugin.name : ""
            body: plugin ? plugin.site : ""
            onFinished: {
                if (value == SystemUiResult.ConfirmButtonSelection){
                    if (plugin.installed && plugin.update){
                        PluginControl.updatePlugin(plugin)
                    }else{
                        PluginControl.installPlugin(plugin)
                    }
                }else if (value == SystemUiResult.CustomButtonSelection){
                    PluginControl.uninstallPlugin(plugin)
                }
            }
            customButton.label: plugin ? plugin.installed ? qsTr("Uninstall") + Retranslate.onLocaleOrLanguageChanged : "" : ""
            confirmButton.label: plugin ? plugin.installed && plugin.update ? qsTr("Update")+ Retranslate.onLocaleOrLanguageChanged : plugin.installed && !plugin.update ? "" : qsTr("Install") + Retranslate.onLocaleOrLanguageChanged : ""
            cancelButton.label: qsTr("Cancel") + Retranslate.onLocaleOrLanguageChanged
        }
    ]
    
    
    Label {
        id: errorLabel
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        multiline: true
        visible: false
        textStyle{
            fontSize: FontSize.PointValue
            fontSizeValue: 15
            color: Color.LightGray
            textAlign: TextAlign.Center
        }
    }
    
    ActivityIndicator {
        preferredHeight: 100
        preferredWidth: 100
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        running: PluginControl.loading
    }
    
    ListView {
        id: listView
        scrollRole: ScrollRole.Main
        /*
        onSelectionChanged: {
            if (selectionList().length > 1) {
                multiSelectHandler.status = selectionList().length + " " + qsTr("items selected") + Retranslate.onLocaleOrLanguageChanged
            } else if (selectionList().length == 1) {
                multiSelectHandler.status = qsTr("1 item selected") + Retranslate.onLocaleOrLanguageChanged;
            } else {
                multiSelectHandler.status = qsTr("None selected") + Retranslate.onLocaleOrLanguageChanged;
            }
        }
        */
        
        opacity: PluginControl.loading || errorLabel.visible ? 0 : 1
        topPadding: 20
        leftPadding: 20
        rightPadding: 20
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        dataModel: ArrayDataModel {
            id: model
        }
        listItemComponents: [
            ListItemComponent {
                type: ""
                ListItemPlugin{}
            }
        ]
        onTriggered: {
            root.indexPath = indexPath
            var item = dataModel.data(indexPath)
            if (item){
                dialog.plugin = item
                dialog.show()
            }
        }
        scrollIndicatorMode: ScrollIndicatorMode.None
        flickMode: FlickMode.Momentum

    } // end of Listview
    
    

}
