import bb.cascades 1.3
import bb 1.0
import com.devpda.tools 1.3
import bb.platform 1.2

import "components"

BasePage {
    id: root
    
    titleText: aboutAction.title
    
    onActiveChanged: {
        if (active){
            payManager.setConnectionMode(1)
            aboutAction.enabled = false
        }else{
            aboutAction.enabled = true
        }
    }
    
    attachedObjects: [
        ApplicationInfo {
            id: info
        },
        Invoker {
            id: invoker
            onInvocationFailed: {
                console.log("Invokation failed")
            }
        },
        PaymentManager {
            id: payManager
            onPurchaseFinished: {
                if (reply.errorCode == 0){
                    Helper.showToast(qsTr("Thanks") + Retranslate.onLanguageChanged)
                }else{
                    console.log("ERROR PAYMENT: ".concat(reply.errorText))
                }
                payContainer.enabled = true
            }
        }
    ]
    
    ScrollView {
        scrollRole: ScrollRole.Main
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties.pinchToZoomEnabled: false
        
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            topPadding: 15
            leftPadding: 15
            rightPadding: 15
            bottomPadding: 15
            
            ImageView {
                horizontalAlignment: HorizontalAlignment.Center
                preferredHeight: 120
                scalingMethod: ScalingMethod.AspectFit
                imageSource: "asset:///images/icon.png"
            }
            
            Label {
                horizontalAlignment: HorizontalAlignment.Center
                textStyle{
                    fontSize: FontSize.PointValue
                    fontSizeValue: 10
                }
                text: "iShrink"
                bottomMargin: 0
            }
            
            Label {
                topMargin: 0
                horizontalAlignment: HorizontalAlignment.Center
                textStyle{
                    fontSize: FontSize.PointValue
                    fontSizeValue: 6
                }
                text: qsTr("Version: %1").arg(info.version) + Retranslate.onLocaleOrLanguageChanged
                bottomPadding: 2
            }

            Divider{bottomMargin: 0; topMargin: 0}
            
            Label {
                topMargin: 2
                horizontalAlignment: HorizontalAlignment.Center
                textStyle{
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                }
                text: qsTr("Keep iShrink free") + Retranslate.onLocaleOrLanguageChanged
                bottomMargin: 2
            }
            
            Container {
                id: payContainer
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                horizontalAlignment: HorizontalAlignment.Fill
                topMargin: 2
                Button {
                    text: "$"
                    onClicked: {
                        payContainer.enabled = false
                        payManager.requestPurchase("56193888", "iShrink-medium")
                    }
                }
                Button {
                    text: "$$"
                    onClicked: {
                        payContainer.enabled = false
                        payManager.requestPurchase("56193889", "iShrink-large")
                    }
                }
            } // end of buttons
            
            Header {
                title: qsTr("Developer") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Label {
                horizontalAlignment: HorizontalAlignment.Center
                textStyle{
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                }
                text: "Zoltán Benke"
                bottomMargin: 0
            }
            Label {
                topMargin: 0
                horizontalAlignment: HorizontalAlignment.Center
                textStyle{
                    fontSize: FontSize.PointValue
                    fontSizeValue: 6
                }
                text: "http://devpda.net"
                textStyle.color: Color.DarkCyan
                onTouch: {
                    Helper.openBrowser(text)
                }
                bottomMargin: 0
            }
            Label {
                topMargin: 0
                multiline: true
                horizontalAlignment: HorizontalAlignment.Center
                textStyle{
                    fontSize: FontSize.PointValue
                    fontSizeValue: 6
                }
                text: qsTr("In case of any problems, please contact me") + Retranslate.onLocaleOrLanguageChanged
                bottomMargin: 0
            }
            Button {
                horizontalAlignment: HorizontalAlignment.Center
                maxHeight: 70
                maxWidth: 70
                imageSource: "asset:///images/mail.png"
                onClicked: {
                    invoker.target = "sys.pim.uib.email.hybridcomposer"
                    invoker.action = "bb.action.SENDEMAIL"
                    invoker.uri = "mailto:support@devpda.net?subject=iShrink (BlackBerry)"
                    invoker.invoke()
                }
            }
            
            Header {
                topMargin: 5
                title: qsTr("Translators") + Retranslate.onLocaleOrLanguageChanged
                subtitle: qsTr("Thanks to") + Retranslate.onLocaleOrLanguageChanged
            } // end of translators header
            
            Label {
                horizontalAlignment: HorizontalAlignment.Center
                text: qsTr("Chinese") + Retranslate.onLocaleOrLanguageChanged + " - jingxuan1993"
                textStyle{
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                }
                bottomMargin: 0
            }                    
        } // end of root container
    } // end of scrollview
}